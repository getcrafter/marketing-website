$(document).ready(function() {
var includes = $('[data-include]');
jQuery.each(includes, function(){
  var file = '/' + $(this).data('include') + '.html';
  $.ajax({
    url: file, 
    success: (data) => {
      $(this).append(data);
    },
    async: false,
  });
});
});